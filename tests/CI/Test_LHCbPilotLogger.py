#!/usr/bin/env python
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import PilotLogger
import PilotLoggerTools
import consumeFromQueue
import os
import json
import unittest
import logging


def dictWithoutKey(d, keyToRemove):
  new_d = d.copy()
  new_d.pop(keyToRemove, None)
  return new_d

def getPilotUUIDFromFile( filename = 'PilotAgentUUID' ):
  """ Retrives Pilot UUID from the file of given name.
  Returns:
    str: empty string in case of errors.
  """

  try:
    with open ( filename, 'r' ) as myFile:
      uniqueId = myFile.read()
    return uniqueId
  except IOError:
    logging.error('Could not open the file!!!')
    return ""

class TestLHCbPilotLogger(unittest.TestCase):

  def test_LHCbPilotLogger(self):
    self.assertEqual(os.system(". ./vm-pilot.sh"), 0)
    uuid = getPilotUUIDFromFile()
    expectedMsgs = [
      {u'status': u'Landed', u'timestamp': u'1463586809.2', u'pilotUUID': u'aa029076-1d10-11e6-b1bd-02163e00d8f2', u'minorStatus': u'Hello I am THE best pilot: LHCb-Production, , ', u'source': u'pilot'},
      {u'status': u'Landed', u'timestamp': u'1463586809.39', u'pilotUUID': u'aa029076-1d10-11e6-b1bd-02163e00d8f2', u'minorStatus': u'Getting DIRAC Pilot 2.0 code from lhcbproject for now...', u'source': u'pilot'},
      {u'status': u'Installing', u'timestamp': u'1463586809.72', u'pilotUUID': u'aa029076-1d10-11e6-b1bd-02163e00d8f2', u'minorStatus': u'Uname      = Linux lbvobox49.cern.ch 2.6.32-573.3.1.el6.x86_64 #1 SMP Fri Aug 14 10:45:09 CEST 2015 x86_64', u'source': u'pilot'},
      {u'status': u'Installing', u'timestamp': u'1463586809.76', u'pilotUUID': u'aa029076-1d10-11e6-b1bd-02163e00d8f2', u'minorStatus': u'Host Name  = lbvobox49.cern.ch', u'source': u'pilot'},
      {u'status': u'Installing', u'timestamp': u'1463586809.78', u'pilotUUID': u'aa029076-1d10-11e6-b1bd-02163e00d8f2', u'minorStatus': u'Host FQDN  = lbvobox49.cern.ch', u'source': u'pilot'},
      {u'status': u'Installing', u'timestamp': u'1463586809.86', u'pilotUUID': u'aa029076-1d10-11e6-b1bd-02163e00d8f2', u'minorStatus': u'WorkingDir = /afs/cern.ch/user/w/wkrzemie/workdir/dirac_development/tests/CI/playground_lhcb/PilotInstallDIR', u'source': u'pilot'}]

    recvMsgs = consumeFromQueue.consume()
    recvMsgs = [json.loads(x) for x in recvMsgs]
    #we get rid of the timestamp field
    expectedMsgs = [dictWithoutKey(x, 'timestamp') for x in expectedMsgs]
    recvMsgs = [dictWithoutKey(x, 'timestamp') for x in recvMsgs]
    #i will remove pilotUUID but that can be loaded from file in the future
    expectedMsgs = [dictWithoutKey(x, 'pilotUUID') for x in expectedMsgs]
    recvMsgs = [dictWithoutKey(x, 'pilotUUID') for x in recvMsgs]
    self.assertEqual(len(expectedMsgs) , len(recvMsgs))
    #first two messages should be identical
    self.assertEqual(expectedMsgs[:2] , recvMsgs[:2])
    for msg in recvMsgs:
      self.assertEqual(msg['source'], 'pilot')
    for msg in recvMsgs[:2]:
      self.assertEqual(msg['status'], 'Landed')
    for msg in recvMsgs[3:]:
      self.assertEqual(msg['status'], 'Installing')

    #list of values of minor status
    recvMinorStatus = [msg['minorStatus'] for msg in recvMsgs]

if __name__ == '__main__':
  unittest.main()
